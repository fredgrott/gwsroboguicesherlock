/*
 * Copyright 2012 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package org.bitbucket.fredgrott.gwsroboguicesherlock.roboguice.activitiy;

import roboguice.RoboGuice;
import roboguice.activity.event.OnActivityResultEvent;
import roboguice.activity.event.OnConfigurationChangedEvent;
import roboguice.activity.event.OnContentChangedEvent;
import roboguice.activity.event.OnCreateEvent;
import roboguice.activity.event.OnDestroyEvent;
import roboguice.activity.event.OnNewIntentEvent;
import roboguice.activity.event.OnPauseEvent;
import roboguice.activity.event.OnRestartEvent;
import roboguice.activity.event.OnResumeEvent;
import roboguice.activity.event.OnStartEvent;
import roboguice.activity.event.OnStopEvent;
import roboguice.event.EventManager;
import roboguice.inject.ContentViewListener;
import roboguice.inject.PreferenceListener;
import roboguice.inject.RoboInjector;
import roboguice.util.RoboContext;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceScreen;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.google.inject.Inject;
import com.google.inject.Key;

import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * An example of how to make your own Robo-enabled Sherlock activity. Feel free
 * to do with with any of the other Sherlock activity types!
 */
public class RoboSherlockPreferenceActivity extends SherlockPreferenceActivity implements RoboContext {
    
    /** The event manager. */
    protected EventManager eventManager;
    
    /** The preference listener. */
    protected PreferenceListener preferenceListener;
    
    /** The scoped objects. */
    protected HashMap<Key<?>, Object> scopedObjects = new HashMap<Key<?>, Object>();

    /** The ignored. */
    @Inject
    ContentViewListener ignored; // BUG find a better place to put this

    /* (non-Javadoc)
     * @see android.preference.PreferenceActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final RoboInjector injector = RoboGuice.getInjector(this);
        eventManager = injector.getInstance(EventManager.class);
        preferenceListener = injector.getInstance(PreferenceListener.class);
        injector.injectMembersWithoutViews(this);
        super.onCreate(savedInstanceState);
        eventManager.fire(new OnCreateEvent(savedInstanceState));
    }

    /**
     * Sets the preference screen.
     *
     * @param preferenceScreen the new preference screen
     * @see android.preference.PreferenceActivity#setPreferenceScreen(android.preference.PreferenceScreen)
     */
    @Override
    public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
        super.setPreferenceScreen(preferenceScreen);
        preferenceListener.injectPreferenceViews();
    }

    /**
     * On restart.
     *
     * @see android.app.Activity#onRestart()
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        eventManager.fire(new OnRestartEvent());
    }

    /**
     * On start.
     *
     * @see android.app.Activity#onStart()
     */
    @Override
    protected void onStart() {
        super.onStart();
        eventManager.fire(new OnStartEvent());
    }

    /**
     * On resume.
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        eventManager.fire(new OnResumeEvent());
    }

    /**
     * On pause.
     *
     * @see com.actionbarsherlock.app.SherlockPreferenceActivity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
        eventManager.fire(new OnPauseEvent());
    }

    /**
     * On new intent.
     *
     * @param intent the intent
     * @see android.preference.PreferenceActivity#onNewIntent(android.content.Intent)
     */
    @Override
    protected void onNewIntent( Intent intent ) {
        super.onNewIntent(intent);
        eventManager.fire(new OnNewIntentEvent());
    }

    /**
     * On stop.
     *
     * @see com.actionbarsherlock.app.SherlockPreferenceActivity#onStop()
     */
    @Override
    protected void onStop() {
        try {
            eventManager.fire(new OnStopEvent());
        } finally {
            super.onStop();
        }
    }

    /**
     * On destroy.
     *
     * @see android.preference.PreferenceActivity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        try {
            eventManager.fire(new OnDestroyEvent());
        } finally {
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                super.onDestroy();
            }
        }
    }

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     * @see com.actionbarsherlock.app.SherlockPreferenceActivity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        eventManager.fire(new OnConfigurationChangedEvent(currentConfig, newConfig));
    }

    /**
     * On content changed.
     *
     * @see android.preference.PreferenceActivity#onContentChanged()
     */
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        RoboGuice.getInjector(this).injectViewMembers(this);
        eventManager.fire(new OnContentChangedEvent());
    }

    /**
     * On activity result.
     *
     * @param requestCode the request code
     * @param resultCode the result code
     * @param data the data
     * @see android.preference.PreferenceActivity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        eventManager.fire(new OnActivityResultEvent(requestCode, resultCode, data));
    }

    /**
     * Gets the scoped object map.
     *
     * @return the scoped object map
     * @see roboguice.util.RoboContext#getScopedObjectMap()
     */
    public Map<Key<?>, Object> getScopedObjectMap() {
        return scopedObjects;
    }
}