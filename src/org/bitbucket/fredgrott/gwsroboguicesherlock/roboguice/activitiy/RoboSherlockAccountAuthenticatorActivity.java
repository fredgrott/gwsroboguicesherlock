package org.bitbucket.fredgrott.gwsroboguicesherlock.roboguice.activitiy;



import org.bitbucket.fredgrott.gwsroboguicesherlock.sherlock.SherlockAccountAuthenticatorActivity;

import roboguice.RoboGuice;
import roboguice.activity.event.*;
import roboguice.event.EventManager;
import roboguice.inject.ContentViewListener;
import roboguice.inject.RoboInjector;
import roboguice.util.RoboContext;

import android.accounts.AccountAuthenticatorActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.google.inject.Inject;
import com.google.inject.Key;



import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * A subclass of {@link AccountAuthenticatorActivity} that provides dependency injection
 * with RoboGuice.
 *
 * @author Marcus Better
 */
public class RoboSherlockAccountAuthenticatorActivity extends SherlockAccountAuthenticatorActivity implements RoboContext {
    
    /** The event manager. */
    protected EventManager eventManager;
    
    /** The scoped objects. */
    protected HashMap<Key<?>, Object> scopedObjects = new HashMap<Key<?>, Object>();

    /** The ignored. */
    @Inject ContentViewListener ignored; // BUG find a better place to put this

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     * @see org.bitbucket.fredgrott.gwsroboguicesherlock.sherlock.SherlockAccountAuthenticatorActivity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final RoboInjector injector = RoboGuice.getInjector(this);
        eventManager = injector.getInstance(EventManager.class);
        injector.injectMembersWithoutViews(this);
        super.onCreate(savedInstanceState);
        eventManager.fire(new OnCreateEvent(savedInstanceState));
    }

    /**
     * On restart.
     *
     * @see android.app.Activity#onRestart()
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        eventManager.fire(new OnRestartEvent());
    }

    /**
     * On start.
     *
     * @see android.app.Activity#onStart()
     */
    @Override
    protected void onStart() {
        super.onStart();
        eventManager.fire(new OnStartEvent());
    }

    /**
     * On resume.
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        eventManager.fire(new OnResumeEvent());
    }

    /**
     * On pause.
     *
     * @see com.actionbarsherlock.app.SherlockActivity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
        eventManager.fire(new OnPauseEvent());
    }

    /**
     * On new intent.
     *
     * @param intent the intent
     * @see android.app.Activity#onNewIntent(android.content.Intent)
     */
    @Override
    protected void onNewIntent( Intent intent ) {
        super.onNewIntent(intent);
        eventManager.fire(new OnNewIntentEvent());
    }

    /**
     * On stop.
     *
     * @see com.actionbarsherlock.app.SherlockActivity#onStop()
     */
    @Override
    protected void onStop() {
        try {
            eventManager.fire(new OnStopEvent());
        } finally {
            super.onStop();
        }
    }

    /**
     * On destroy.
     *
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        try {
            eventManager.fire(new OnDestroyEvent());
        } finally {
            try {
                RoboGuice.destroyInjector(this);
            } finally {
                super.onDestroy();
            }
        }
    }

    /**
     * On configuration changed.
     *
     * @param newConfig the new config
     * @see com.actionbarsherlock.app.SherlockActivity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final Configuration currentConfig = getResources().getConfiguration();
        super.onConfigurationChanged(newConfig);
        eventManager.fire(new OnConfigurationChangedEvent(currentConfig, newConfig));
    }

    /**
     * On content changed.
     *
     * @see android.app.Activity#onContentChanged()
     */
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        RoboGuice.getInjector(this).injectViewMembers(this);
        eventManager.fire(new OnContentChangedEvent());
    }

    /**
     * On activity result.
     *
     * @param requestCode the request code
     * @param resultCode the result code
     * @param data the data
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        eventManager.fire(new OnActivityResultEvent(requestCode, resultCode, data));
    }

    /**
     * Gets the scoped object map.
     *
     * @return the scoped object map
     * @see roboguice.util.RoboContext#getScopedObjectMap()
     */
    public Map<Key<?>, Object> getScopedObjectMap() {
        return scopedObjects;
    }
}