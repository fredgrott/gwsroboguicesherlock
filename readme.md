GWSRoboguiceSherlock
---

# Intoruction

Forked from rtyley's roboguice-sherlock(at github). Makes it easier to use 
roboguice with ActionBarSherlock without the pain.

# License

Apache 2 License.
